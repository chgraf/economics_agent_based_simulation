CXX=g++
CFLAGS=-c -Wall -std=c++11

RM=rm -f

SOURCES=main.cpp
INCLUDES=-I/usr/local/include/boost_1_58_0
LIBRARIES= -lboost_system -lboost_filesystem
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=main

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@

.cpp.o:
	$(CXX) $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	$(RM) $(EXECUTABLE) $(OBJECTS)
