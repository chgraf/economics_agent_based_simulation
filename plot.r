# Author: Christian Graf
# Date: November 2015
#
# basic data analysis of simulation output
#


library("ggplot2")
library("reshape2")
library("MASS")

# Color housekeeping
library(RColorBrewer)
rf <- colorRampPalette(rev(brewer.pal(11,'Spectral')))
color_r <- rf(32)

setwd("./data/scan_1")
run <- 913
path <- sprintf("./run_%i",run)

# -------- read config --------
file <- paste(path,"config.dat", sep="/")
config <- read.table(file, row.names = 1)
config
nExperiments <- config['nExperiments',]
nRounds <- config['nRounds', ]
nPlayers <- config['nPlayers', ]
nGames <- config['nGames', ]
games_per_player <- config['games_per_player', ]
game_assignment_mode <- config['game_assignment_mode', ]
pCoop <- config['pCoop', ]
costs_min <- config['costs_min', ]
costs_max <- config['costs_max', ]
benefit_min <- config['benefit_min', ]
benefit_max <- config['benefit_max', ]
nNeighbours_random <- config['nNeighbours_random', ]
updateStrength <- config['updateStrength', ]
updateAversion <- config['updateAversion', ]
neighbours_mode <- config['neighbours_mode', ]

# -------- update curve --------
#updateAversion = 20
#updateStrength = 10
eq = function(x){1/(1+updateAversion*exp(x*updateStrength))}
qplot(c(-1,1), fun=eq, stat="function", geom="line", xlab=expression(paste(Delta,Pi)), ylab="P(x->y)") +
    theme_bw() +
    theme(axis.title.x = element_text(size=30, vjust=-0.4), axis.text.x = element_text(size=30), 
          axis.title.y = element_text(size=30, vjust=1.5), axis.text.y = element_text(size=30))

# -------- cooperators --------
file <- paste(path,"cooperators.dat", sep="/")
cooperators <- read.table(file, header=TRUE)
#plot(cooperators$round, cooperators$cooperators)
ggplot(dat=cooperators, aes(x=round,y=cooperators)) +
  geom_point() +
  scale_x_continuous(limits = c(0, 500)) +
  scale_y_continuous(limits = c(0, 1)) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        axis.title.x = element_text(size=25, vjust=-0.4), axis.text.x = element_text(size=25), 
        axis.title.y = element_text(size=25, vjust=1.5), axis.text.y = element_text(size=25))
#s_cooperators <- cooperators[0:nrow(cooperators),]
#plot(s_cooperators$round, s_cooperators$cooperators)
#fit <- lm(s_cooperators$cooperators~s_cooperators$round)
#abline(fit, col="red")
#summary(fit)

#ggplot(data=cooperators, aes(round)) +
#  geom_line(aes(y = pNeighboursCC, colour = "pNeighboursCC")) +
#  geom_line(aes(y = pNeighboursCD + pNeighboursCC, colour = "pNeighboursCD")) +
#  geom_line(aes(y = pNeighboursDD + pNeighboursCD + pNeighboursCC, colour = "pNeighboursDD")) +
#  geom_line(aes(y = pNeighboursDC + pNeighboursDD + pNeighboursCD + pNeighboursCC, colour = "pNeighboursDC"))

ggplot(data=cooperators, aes(round)) +
  geom_line(aes(y = pNeighboursCC/(cooperators*cooperators), colour = "CC")) +
  geom_line(aes(y = pNeighboursCD/(cooperators*(1-cooperators)), colour = "CD")) +
  geom_line(aes(y = pNeighboursDD/((1-cooperators)*(1-cooperators)), colour = "DD")) +
  geom_line(aes(y = pNeighboursDC/(cooperators*(1-cooperators)), colour = "DC")) +
  geom_line(aes(y = cooperators, colour = "cooperators"), colour="black", lwd=1.2) +
  labs(x = "Round", y = "Cluster Coefficient", color = "Cluster")

# -------- games --------
allgames = NULL
for(iExp in 0:(nExperiments-1)) {
  expPath <- sprintf("exp_%i",iExp)
  expPath <- paste(path,expPath, sep="/")
  file <- paste(expPath,"games.dat", sep="/")
  games <- read.table(file, header=TRUE)
  allgames <- rbind(allgames, games)
}
#plot(allgames$nPlayers, allgames$Coop)
#plot(allgames$costs, allgames$Coop)
ggplot(allgames, aes(x=costs, y=Coop)) +
  stat_bin2d() + 
  scale_fill_gradientn(colours=color_r) + #, trans="log")
  theme_bw() +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        panel.background = element_rect(colour = "black"),
        axis.title.x = element_text(size=25, vjust=-0.4), axis.text.x = element_text(size=20), 
        axis.title.y = element_text(size=25, vjust=1.5), axis.text.y = element_text(size=20),
        legend.text=element_text(size=20),
        legend.title=element_text(size=20))
#ggplot(data=allgames, aes(allgames$endCoop)) +
#  geom_histogram(aes(x = allgames$endCoop))
#hist(allgames$nPlayers, breaks = 18, probability = FALSE)

# -------- players --------

expPath <- sprintf("exp_%i",0)
expPath <- paste(path,expPath, sep="/")
file <- paste(expPath,"players.dat", sep="/")
players <- read.table(file, header=TRUE)
lattice <- melt(matrix(data = players$endCoop, nrow = sqrt(nPlayers), ncol=sqrt(nPlayers)))
ggplot(data = lattice, aes(x=Var2,y=Var1, fill=factor(value))) +
  labs(x="x", y="y") +
  geom_raster() +
  coord_equal() +
  #scale_fill_gradient(breaks=c(0,1),low="steelblue", high="lightyellow") +
  scale_fill_manual(values=c("steelblue","lightyellow"),
                    name="Legend",
                    breaks=c(0,1),
                    labels=c("D","C")) +
  theme_bw()

lattice <- melt(matrix(data = players$meanCoop, nrow = sqrt(nPlayers), ncol=sqrt(nPlayers)))
ggplot(data = lattice, aes(x=Var2,y=Var1, fill=value)) +
  labs(x="x", y="y") +
  geom_raster() +
  coord_equal() +
  scale_fill_gradient(low="steelblue", high="lightyellow") +
  #scale_fill_manual(values=c("steelblue","lightyellow"),
   #                 name="Legend",
    #                breaks=c(0,1),
     #               labels=c("D","C")) +
  theme_bw()


#image(matrix(data = players$endCoop, nrow = sqrt(nPlayers), ncol=sqrt(nPlayers)))

allplayers = NULL
for(iExp in 0:(nExperiments-1)) {
  expPath <- sprintf("exp_%i",iExp)
  expPath <- paste(path,expPath, sep="/")
  file <- paste(expPath,"players.dat", sep="/")
  tmp_players <- read.table(file, header=TRUE)
  allplayers <- rbind(allplayers, tmp_players)
}

ggplot(data=allplayers, aes(allplayers$meanCoop)) +
  geom_histogram(aes(x = allplayers$meanCoop))
ggplot(data=allplayers, aes(allplayers$nUpdates)) +
  geom_histogram(aes(x = allplayers$nUpdates))
#hist(allgames$nPlayers, breaks = 18, probability = FALSE)

# -------- update Details --------
allUpdates = NULL
expPath <- sprintf("exp_%i",iExp)
expPath <- paste(path,expPath, sep="/")
myfile <- paste(expPath,"updateDetails.dat", sep="/")
if(file.exists(myfile)) {
  for(iExp in 0:(nExperiments-1)) {
    expPath <- sprintf("exp_%i",iExp)
    expPath <- paste(path,expPath, sep="/")
    myfile <- paste(expPath,"updateDetails.dat", sep="/")
    updates <- read.table(myfile, header=TRUE)
    allUpdates <- rbind(allUpdates, updates)
  }
  #plot(allUpdates)
  #hist(allUpdates$payoffDiff[which(allUpdates$switch == 1)])
  #hist(allUpdates$payoffDiff)
  ggplot(data=allUpdates, aes(x=payoffDiff)) +
    geom_histogram(aes(y=(..count..)/1000), stat="bin", colour="black",fill="lightyellow") +
    geom_line(dat=subset(allUpdates, allUpdates$switch == 0), aes(x=payoffDiff, y=(..count..)/1000), stat="bin", colour="black", lwd=2) +
    geom_line(dat=subset(allUpdates, allUpdates$switch == 1), aes(x=payoffDiff, y=(..count..)/1000), stat="bin", colour="black", lwd=2) +
    geom_line(aes(x=payoffDiff, y=(..count..)/1000, colour=factor(allUpdates$switch) ),stat="bin",lwd=1.5) +
    labs(x= "payoff difference", y="counts / 1000") + 
    scale_colour_manual(breaks=c(0,1),
                        values=c("steelblue","darkolivegreen3"),
                        name="switched\n strategy",
                        labels=c("no","yes")) +
    theme_bw() +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          panel.background = element_rect(colour = "black"),
          axis.title.x = element_text(size=25, vjust=-0.4), axis.text.x = element_text(size=20), 
          axis.title.y = element_text(size=25, vjust=1.5), axis.text.y = element_text(size=20),
          legend.text=element_text(size=20),
          legend.title=element_text(size=20))
}
