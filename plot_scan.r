# Author: Christian Graf
# Date: November 2015
#
# advanced data analysis of simulation output
# producing scan plots

library("ggplot2")
library("MASS")

setwd("./data/scan_1")


run <- 850

# -------- read config --------
path <- sprintf("./run_%i",run)
file <- paste(path,"config.dat", sep="/")

config <- read.table(file, row.names = 1)
config
nExperiments <- config['nExperiments',]
nRuns <- config['nRuns',]
nRounds <- config['nRounds', ]
nPlayers <- config['nPlayers', ]
nGames <- config['nGames', ]
games_per_player <- config['games_per_player', ]
game_assignment_mode <- config['game_assignment_mode', ]
pCoop <- config['pCoop', ]
costs_min <- config['costs_min', ]
costs_max <- config['costs_max', ]
scan_step_costs <- config['scan_step_costs',]
benefit_min <- config['benefit_min', ]
benefit_max <- config['benefit_max', ]
nNeighbours_random <- config['nNeighbours_random', ]
updateStrength <- config['updateStrength', ]
updateAversion <- config['updateAversion', ]
neighbours_mode <- config['neighbours_mode', ]
updateRule <- config['strategy_update_mode', ]

# -------- scan costs plot --------
vMeanCooperators = NULL
for(iScan in 0:(nRuns-1)) {
  path <- sprintf("./run_%i",run)
  file <- paste(path,"cooperators.dat", sep="/")
  cooperators <- read.table(file, header=TRUE)
  #plot(cooperators$round, cooperators$cooperators)
  s_cooperators <- cooperators[nrow(cooperators)-200:nrow(cooperators),]
  vMeanCooperators = c(vMeanCooperators,mean(s_cooperators$cooperators))
  run = run+1
}
dat <- data.frame(costs=seq(from=(costs_min+costs_max)/2, to=(costs_min+costs_max)/2+scan_step_costs*(nRuns-1), by=scan_step_costs), 
  coop=vMeanCooperators)


# -------- theory! --------
A <- updateAversion
S <- 1/updateStrength
b <- benefit_min
nP = nPlayers / nGames * games_per_player
# immitate best
if(updateRule == 0) {
  eq = function(x){0.5 * (1 + sign(x)) }
}
if(updateRule == 3) {
  #A <- 5
  #S <- 1/5
   # eq = function(x){1 - (A+exp((b/nP-x)/S)) / (A*exp(2*(b/nP-x)/S) + A + 2*exp(2*(b/nP-x)/S))}
  eq = function(x){1 - (A+exp((x)/S)) / (A*exp(2*(x)/S) + A + 2*exp((x)/S))}
}
#qplot(c(costs_min,costs_min+scan_step_costs*nRuns), fun=eq, stat="function", geom="line", xlab=expression(paste(Delta,Pi)), ylab="P(x->y)")
#  theme_bw() +
#  theme(axis.title.x = element_text(size=30, vjust=-0.4), axis.text.x = element_text(size=30), 
#        axis.title.y = element_text(size=30, vjust=1.5), axis.text.y = element_text(size=30))

ggplot(data=dat, aes(x=(b/nP-costs),y=coop)) +
  stat_function(fun=eq, colour="black") +
  geom_point(colour="steelblue", size=5) +
  labs(x=expression(eta), y=expression(paste('cooperators ', paste(rho,''[C])))) + 
    theme_bw() +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          panel.background = element_rect(colour = "black"),
          axis.title.x = element_text(size=25, vjust=-0.4), axis.text.x = element_text(size=20), 
          axis.title.y = element_text(size=25, vjust=1.5), axis.text.y = element_text(size=20))
