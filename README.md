# Agent Based Simulation of an Evolutionary Public Goods Game with Spatial Structure

Part of economics master thesis: *Evolutionary Games on Multilayer Networks with Heterogeneous
Payoffs*

### Implementation
The simulation is implemented in C++ and the results are written as tab separated data. The data is
further analysed by R scripts to produce the plots shown in the thesis.

### Requirements
cpp11
boost 1.58

Author: Christian Graf
Date November 2015

