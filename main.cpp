/* Author: Christian Graf
 * Date: November 2015
 *
 * Project: Economics master thesis
 *
 * This code implements an agent based monte carlo simulation
 * of a public goods game
 */



#include <algorithm>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <queue>
#include <set>
#include <sstream>
#include <vector>

#include <assert.h>

#include <cmath>
#include <ctime>

#include <sys/stat.h>

#include <boost/random.hpp>
//#define BOOST_NO_CXX11_SCOPED_ENUMS
//#include <boost/filesystem.hpp>

using namespace std;

const bool DEBUG = true;

//initialize random
typedef boost::mt19937 RNGType;
time_t now = time(0); //time for random seed
RNGType rng(now);      //pseudo random number generator. Insert different seed for different results
boost::random::uniform_real_distribution<> rnd_uniform(0,1); //uniform distribution between 0 and 1

//empty struct, so that Player knows PGG
struct PGG;
//the players
struct Player {
    int ID = 0;
    float payoff = 0;               //players payoff in current round
    float costs_bias = 0;
    bool cooperate = false;         //strategy
    set<PGG*> sPGG;                 //vector of Public good games the player participates
    set<Player*> sNeighbours;       //vector of neighbours where the player updates its strategy
    PGG* focal_game;                 //main game of player
};

//public goods game
struct PGG {
    int ID = 0;
    float costs = 0.;           //costs for cooperating
    float benefit = 1.;         //benefit if somebody cooperates
    set<Player*> sPlayers;      //vector of players who play the game
};

// SAMEGAME: Neighbours are everybdy the player played with
// RANDOM: Choose random neighbours
// FOCAL_GAME: Chooses neighbours just from one "focal" game, makes most sense with lattice config
enum class Neighbours_Mode {
    SAMEGAME, RANDOM, FOCAL_GAME
};

// EQUAL: Everybody has the same amount of games
// EQUAL_BIASED: two types, one with few games, one with more games
// POISSON: Number of games is Poisson distributed
// POISSON_BIASED: Number of games is Poisson distributed, following two distributions
enum class Game_Assignment_Mode {
    EQUAL, EQUAL_BIASED, POISSON, POISSON_BIASED, LATTICE
};

// RANDOM: Ask a random neighbour, compare payoffs, decide to adopt
// SAMEPROB: Ask everypody for payoffs, decide to adopt from sum of probabilities
// SMOOTHED_IMMITATION: Sum all C's and all D's and then decide to adopt C or D
enum class Strategy_Update_Mode {
    BEST_IMITATION, RANDOM, SAMEPROB, SMOOTHED_IMMITATION, SMOOTHED_IMMITATION_SINGLE
};

//SINGLE_CONTRIBUTION: Everybody who contributes pays costs, but benefit always = 1 if at least
//                      someone cooperates
//MULTI_CONTRIBUTION: Everybody can contribute and benefits
//                      => Fraction of cooperators is important
enum class Game_Mode {
    SINGLE_CONTRIBUTION, MULTI_CONTRIBUTION
};

//holds all configuration variables
namespace config {
    int run, scan;
    int nPlayers, nGames, nRounds, nExperiments, nRuns;  //Number of players
    int games_per_player;  //Number of average games a player participates in
    float pCoop;                    //Initial probability to cooperate
    float costs_min, costs_max, benefit_min, benefit_max, costs_het; //variables for PGG
    bool f_het_c_games, f_het_c_players;
    float scan_step_costs;
    float updateStrength, updateAversion;
    float nNeighbours_random;
    Neighbours_Mode neighbours_mode;
    Game_Assignment_Mode game_assignment_mode;
    Game_Mode game_mode;
    Strategy_Update_Mode strategy_update_mode;
    stringstream ss_path;
}

//holds extra statistics
namespace stats {
    //stats relevant for one experiment
    class Round;
    class Experiment {
        public:
            int initial_cooperators = 0;
            float pgg_effective_costs = 0.;
            float pgg_effective_benefit = 0.;
            float mean_games_per_player = 0.;
            float mean_players_per_game = 0.;
            float mean_nNeighbours = 0;
            float nCooperators_stable = 0.;
            float mean_cooperators = 0;

            vector<Round> vR;
            vector<int> vCooperators;
            vector<float> vPayoffs;

            vector<double> mean_cooperators_per_game; //vector with length nGames
            vector<int> updates_per_player; //vector with length nGames
            vector<double> mean_cooperates_per_player; //vector with length nGames
    };

    vector<Experiment> vExp;

    //stats relevant for one round
    class Round {
        public:
            int nPGG_with_cooperator = 0;
            int nCooperators = 0;
            float mean_payoff = 0.;
            float mean_payoff_C = 0.;
            float mean_payoff_D = 0.;
            int switch_to_cooperate = 0;
            int switch_to_defect = 0;
            float pNeighbourCC = 0.;
            float pNeighbourCD = 0.;
            float pNeighbourDC = 0.;
            float pNeighbourDD = 0.;

            vector<double> cooperators_per_game; //vector with length nGames
    };

    Experiment* this_exp;
    Round* this_round;

    vector<double> vMean_cooperators; //# cooperators averaged over all experiments per round

    bool fWriteUpdt = false;

    void reset() {
        vExp.clear();
        vMean_cooperators.clear();
    }

}

const char* WD = "/Users/christian/Documents/Programmieren/egt_network/";
//const char* WD = "./";

int iExp = 0;
int iR = 0;

namespace debug {
    bool config = true;
    bool experiment_stats = true;
    bool round_stats = true;
    bool round_strategy_updates = false;
    bool game_details = false;
    //bool round_strategy_updates_detailed = true;
}

//generates players given the total number of players and the inital fraction of cooperators
//returns: vector of all players
// O(nPlayers)
vector<Player> generatePlayers(unsigned int nPlayers, float pCoop) {
    vector<Player> vPlayers;
    Player p;
    //initialize bernoulli distribution for deciding the strategy
    boost::random::bernoulli_distribution<> rnd_bernoulli(pCoop);
    stats::this_exp->initial_cooperators = 0;

    for(int i=0; i < nPlayers; i++) {
        stats::this_exp->updates_per_player.push_back(0);
        stats::this_exp->mean_cooperates_per_player.push_back(0);
        p.ID = i;
        if(rnd_bernoulli(rng) == 1) { //roll the die
            stats::this_exp->initial_cooperators++;
            p.cooperate = true;
        } else {
            p.cooperate = false;
        }
        // Add bias if heterogeneous costs
        if(config::f_het_c_players) {
            p.costs_bias = config::costs_het*(2*rnd_uniform(rng) - 1);
        } else {
            p.costs_bias = 0;
        }

        vPlayers.push_back(p);
    }
    return vPlayers;
}

vector<Player> initialCoopDistribution(vector<Player>& vPlayers) {
    if(config::game_assignment_mode != Game_Assignment_Mode::LATTICE) {
        cerr << "Error: No lattice!!!" << endl;
    }

    int n = sqrt(config::nPlayers);
    for(auto& p: vPlayers) {
        p.cooperate = false;
    }


    /*
    vPlayers[n*(n/2-2)+n/2-2].cooperate = true;
    vPlayers[n*(n/2-2)+n/2-1].cooperate = true;
    vPlayers[n*(n/2-2)+n/2].cooperate = true;
    vPlayers[n*(n/2-2)+n/2+1].cooperate = true;
    vPlayers[n*(n/2-2)+n/2+2].cooperate = true;
    vPlayers[n*(n/2-1)+n/2-2].cooperate = true;
    vPlayers[n*(n/2-1)+n/2-1].cooperate = true;
    vPlayers[n*(n/2-1)+n/2].cooperate = true;
    vPlayers[n*(n/2-1)+n/2+1].cooperate = true;
    vPlayers[n*(n/2-1)+n/2+2].cooperate = true;
    vPlayers[n*n/2+n/2-2].cooperate = true;
    vPlayers[n*n/2+n/2-1].cooperate = true;
    vPlayers[n*n/2+n/2].cooperate = true;
    vPlayers[n*n/2+n/2+1].cooperate = true;
    vPlayers[n*n/2+n/2+2].cooperate = true;
    vPlayers[n*(n/2+1)+n/2-2].cooperate = true;
    vPlayers[n*(n/2+1)+n/2-1].cooperate = true;
    vPlayers[n*(n/2+1)+n/2].cooperate = true;
    vPlayers[n*(n/2+1)+n/2+1].cooperate = true;
    vPlayers[n*(n/2+1)+n/2+2].cooperate = true;
    vPlayers[n*(n/2+2)+n/2-2].cooperate = true;
    vPlayers[n*(n/2+2)+n/2-1].cooperate = true;
    vPlayers[n*(n/2+2)+n/2].cooperate = true;
    vPlayers[n*(n/2+2)+n/2+1].cooperate = true;
    vPlayers[n*(n/2+2)+n/2+2].cooperate = true;
    */
    vPlayers[n*(n/2-2)+n/2-1+5].cooperate = true;
    vPlayers[n*(n/2-2)+n/2+5].cooperate = true;
    vPlayers[n*(n/2-1)+n/2-1+5].cooperate = true;
    vPlayers[n*(n/2-1)+n/2+5].cooperate = true;
    vPlayers[n*(n/2-1)+n/2+1+5].cooperate = true;
    vPlayers[n*n/2+n/2-1+5].cooperate = true;
    vPlayers[n*n/2+n/2+5].cooperate = true;
    vPlayers[n*n/2+n/2+1+5].cooperate = true;
    vPlayers[n*(n/2+1)+n/2-1+5].cooperate = true;
    vPlayers[n*(n/2+1)+n/2+5].cooperate = true;
    vPlayers[n*(n/2+1)+n/2+1+5].cooperate = true;
    vPlayers[n*(n/2+2)+n/2+5].cooperate = true;

    vPlayers[n*(n/2-1)+n/2-1-5].cooperate = true;
    vPlayers[n*(n/2-1)+n/2-5].cooperate = true;
    vPlayers[n*(n/2-1)+n/2+1-5].cooperate = true;
    vPlayers[n*n/2+n/2-1-5].cooperate = true;
    vPlayers[n*n/2+n/2-5].cooperate = true;
    vPlayers[n*n/2+n/2+1-5].cooperate = true;
    vPlayers[n*(n/2+1)+n/2-1-5].cooperate = true;
    vPlayers[n*(n/2+1)+n/2-5].cooperate = true;
    vPlayers[n*(n/2+1)+n/2+1-5].cooperate = true;

    vPlayers[n*(n/2+10)+n/2+1+10].cooperate = true;
    vPlayers[n*(n/2+10)+n/2+1-10].cooperate = true;
    vPlayers[n*(n/2+10)+n/2+1-11].cooperate = true;
    vPlayers[n*(n/2-10)+n/2+1-10].cooperate = true;
    vPlayers[n*(n/2-10)+n/2+1-9].cooperate = true;
    vPlayers[n*(n/2-10)+n/2+1-11].cooperate = true;
    vPlayers[n*(n/2-10)+n/2+1+8].cooperate = true;
    vPlayers[n*(n/2-10)+n/2+1+9].cooperate = true;
    vPlayers[n*(n/2-10)+n/2+1+10].cooperate = true;
    vPlayers[n*(n/2-10+1)+n/2+1+10].cooperate = true;

    vPlayers[n*(n/2-10)+n/2].cooperate = true;
    vPlayers[n*(n/2-10)+n/2+1].cooperate = true;
    vPlayers[n*(n/2-10)+n/2-1].cooperate = true;
    vPlayers[n*(n/2-10+1)+n/2].cooperate = true;
    vPlayers[n*(n/2-10-1)+n/2].cooperate = true;

    vPlayers[n*(n/2+10)+n/2+1].cooperate = true;
    vPlayers[n*(n/2+10+1)+n/2+1].cooperate = true;
    vPlayers[n*(n/2+10+1)+n/2].cooperate = true;
    vPlayers[n*(n/2+10+1)+n/2-1].cooperate = true;
    vPlayers[n*(n/2+10+2)+n/2+1].cooperate = true;
    vPlayers[n*(n/2+10+2)+n/2].cooperate = true;
    vPlayers[n*(n/2+10+2)+n/2-1].cooperate = true;
    vPlayers[n*(n/2+10)+n/2-1].cooperate = true;
    vPlayers[n*(n/2+10-1)+n/2-1].cooperate = true;
    vPlayers[n*(n/2+10-1)+n/2].cooperate = true;
    vPlayers[n*(n/2+10-1)+n/2+1].cooperate = true;
    vPlayers[n*(n/2+10-2)+n/2-1].cooperate = true;
    vPlayers[n*(n/2+10-2)+n/2].cooperate = true;
    vPlayers[n*(n/2+10-2)+n/2+1].cooperate = true;

    return vPlayers;
}


//generates games given its parameter spaces
//returns vector of all games
// O(nGames)
vector<PGG> generateGames(unsigned int nGames, float costs_min, float costs_max,
                            float benefit_min, float benefit_max) {
    vector<PGG> vGames;
    PGG g;

    stats::this_exp->pgg_effective_costs = 0.;
    stats::this_exp->pgg_effective_benefit = 0.;

    for(int i=0; i < nGames; i++) {
        //use random numbers from 0..1 to generate an arbitrary intervall of
        //random numbers
        g.ID = i;
        g.costs = (costs_max-costs_min)*rnd_uniform(rng)+costs_min;
        g.benefit = (benefit_max-benefit_min)*rnd_uniform(rng)+benefit_min;
        stats::this_exp->pgg_effective_costs += g.costs;
        stats::this_exp->pgg_effective_benefit += g.benefit;

        vGames.push_back(g);

        //initialize mean_cooperators_per_game
        stats::this_exp->mean_cooperators_per_game.push_back(0);
    }

    stats::this_exp->pgg_effective_costs = stats::this_exp->pgg_effective_costs/nGames;
    stats::this_exp->pgg_effective_benefit = stats::this_exp->pgg_effective_benefit/nGames;
    if(DEBUG) {
        cout << "Effective costs: " << stats::this_exp->pgg_effective_costs << "\t";
        cout << "Effective benefit: " << stats::this_exp->pgg_effective_benefit << endl;
    }

    return vGames;
}

// O(nPlayers * games_per_player)
void assignPlayersToGames(vector<Player>* vPlayers, vector<PGG>* vGames, int games_per_player,
                Game_Assignment_Mode Mode) {
    stats::this_exp->mean_games_per_player = 0;

    //Build a lattice
    if(Mode == Game_Assignment_Mode::LATTICE) {
        if(config::nPlayers != config::nGames) {
            cerr << "Error: For the lattice configuration, nPlayers = nGames! nPlayers: " << config::nPlayers;
            cerr << "\tnGames: " << config::nGames << endl; exit(1);
        }

        if(sqrt((float)config::nPlayers) != floor(sqrt((float)config::nPlayers))) {
            cerr << "Error: For the lattice configuration, nPlayers has to be a perfect square: " << config::nPlayers;
            cerr << "\tsqrt(nPlayers): " << sqrt(config::nPlayers) << endl; exit(1);
        }

        int nRows = sqrt(config::nPlayers);
        int corner_lo = 0;
        int corner_ro = nRows-1;
        int corner_lu = config::nPlayers-nRows;
        int corner_ru = config::nPlayers-1;


        for(Player& player: *vPlayers) {
            //cout << "Player ID: " << player.ID << endl;
            (*vGames)[player.ID].sPlayers.insert(&player);
            player.sPGG.insert(&(*vGames)[player.ID]);
            player.focal_game = &(*vGames)[player.ID];
            if(player.ID == corner_lo) {
                player.sPGG.insert(&(*vGames)[1]);
                player.sPGG.insert(&(*vGames)[nRows]);
                player.sPGG.insert(&(*vGames)[corner_ru]);
                player.sPGG.insert(&(*vGames)[corner_lu]);
                (*vGames)[1].sPlayers.insert(&player);
                (*vGames)[nRows].sPlayers.insert(&player);
                (*vGames)[corner_ru].sPlayers.insert(&player);
                (*vGames)[corner_lu].sPlayers.insert(&player);
            } else if(player.ID == corner_ro) {
                player.sPGG.insert(&(*vGames)[corner_ro+1]);
                player.sPGG.insert(&(*vGames)[corner_ro+nRows]);
                player.sPGG.insert(&(*vGames)[corner_ro-1]);
                player.sPGG.insert(&(*vGames)[corner_ru]);
                (*vGames)[corner_ro+1].sPlayers.insert(&player);
                (*vGames)[corner_ro+nRows].sPlayers.insert(&player);
                (*vGames)[corner_ro-1].sPlayers.insert(&player);
                (*vGames)[corner_ru].sPlayers.insert(&player);
            } else if(player.ID == corner_lu) {
                player.sPGG.insert(&(*vGames)[corner_lu+1]);
                player.sPGG.insert(&(*vGames)[corner_lo]);
                player.sPGG.insert(&(*vGames)[corner_lu-1]);
                player.sPGG.insert(&(*vGames)[corner_lu-nRows]);
                (*vGames)[corner_lu+1].sPlayers.insert(&player);
                (*vGames)[corner_lo].sPlayers.insert(&player);
                (*vGames)[corner_lu-1].sPlayers.insert(&player);
                (*vGames)[corner_lu-nRows].sPlayers.insert(&player);
            } else if(player.ID == corner_ru) {
                player.sPGG.insert(&(*vGames)[corner_lo]);
                player.sPGG.insert(&(*vGames)[corner_ro]);
                player.sPGG.insert(&(*vGames)[corner_ru-1]);
                player.sPGG.insert(&(*vGames)[corner_ru-nRows]);
                (*vGames)[corner_lo].sPlayers.insert(&player);
                (*vGames)[corner_ro].sPlayers.insert(&player);
                (*vGames)[corner_ru-1].sPlayers.insert(&player);
                (*vGames)[corner_ru-nRows].sPlayers.insert(&player);
            } else if(player.ID < nRows) {
                player.sPGG.insert(&(*vGames)[player.ID+1]);
                player.sPGG.insert(&(*vGames)[player.ID+nRows]);
                player.sPGG.insert(&(*vGames)[player.ID-1]);
                player.sPGG.insert(&(*vGames)[config::nPlayers-nRows+player.ID]);
                (*vGames)[player.ID+1].sPlayers.insert(&player);
                (*vGames)[player.ID+nRows].sPlayers.insert(&player);
                (*vGames)[player.ID-1].sPlayers.insert(&player);
                (*vGames)[config::nPlayers-nRows+player.ID].sPlayers.insert(&player);
            } else if(player.ID >= config::nPlayers-nRows) {
                player.sPGG.insert(&(*vGames)[player.ID+1]);
                player.sPGG.insert(&(*vGames)[player.ID+nRows-config::nPlayers]);
                player.sPGG.insert(&(*vGames)[player.ID-1]);
                player.sPGG.insert(&(*vGames)[player.ID-nRows]);
                (*vGames)[player.ID+1].sPlayers.insert(&player);
                (*vGames)[player.ID+nRows-config::nPlayers].sPlayers.insert(&player);
                (*vGames)[player.ID-1].sPlayers.insert(&player);
                (*vGames)[player.ID-nRows].sPlayers.insert(&player);
            } else {
                player.sPGG.insert(&(*vGames)[player.ID+1]);
                player.sPGG.insert(&(*vGames)[player.ID+nRows]);
                player.sPGG.insert(&(*vGames)[player.ID-1]);
                player.sPGG.insert(&(*vGames)[player.ID-nRows]);
                (*vGames)[player.ID+1].sPlayers.insert(&player);
                (*vGames)[player.ID+nRows].sPlayers.insert(&player);
                (*vGames)[player.ID-1].sPlayers.insert(&player);
                (*vGames)[player.ID-nRows].sPlayers.insert(&player);
            }
        }
        stats::this_exp->mean_games_per_player = 5;
    } else { //No Lattice Mode

        boost::random::uniform_int_distribution<> rnd_game(0,config::nGames-1);
        //generate random numbers:
        double bias = 0.99;
        int few_games = max((int)(games_per_player / 2),2);
        int more_games = 1./(1.-bias)*(games_per_player-few_games*bias);
        boost::random::poisson_distribution<> rnd_poisson_0(max(games_per_player-1,1));
        boost::random::poisson_distribution<> rnd_poisson_1(max(few_games-1,1));
        boost::random::poisson_distribution<> rnd_poisson_2(max(more_games - 1,1));
        boost::random::bernoulli_distribution<> rnd_bernoulli_poisson(bias);
        boost::random::bernoulli_distribution<> rnd_bernoulli_equal(bias);
        vector<int> v_nGames_per_player;
        //Defines the number of links of a player
        for(int i=0; i<vPlayers->size(); i++) {
            if(Mode == Game_Assignment_Mode::EQUAL) {
                //a) every player plays the same amount of games
                v_nGames_per_player.push_back(games_per_player);
            } else if (Mode == Game_Assignment_Mode::EQUAL_BIASED){
                if(rnd_bernoulli_equal(rng)) {
                    v_nGames_per_player.push_back(few_games);
                } else {
                    v_nGames_per_player.push_back(more_games);
                }
            } else if(Mode == Game_Assignment_Mode::POISSON) {
                //b) use poissonian distribution (1 + poisson(k-1))
                v_nGames_per_player.push_back(1 + rnd_poisson_0(rng));
            } else if(Mode == Game_Assignment_Mode::POISSON_BIASED) {
                //c) Use two poisson distributions
                if(rnd_bernoulli_poisson(rng)) {
                    v_nGames_per_player.push_back(1 + rnd_poisson_1(rng));
                } else {
                    v_nGames_per_player.push_back(1 + rnd_poisson_2(rng));
                }
            }
        }

        bool fix_players_per_game = false;
        if(Mode == Game_Assignment_Mode::EQUAL) {
            fix_players_per_game = true;
        }

        int players_per_game = 0;
        //vector<vector<PGG*> > shuffeled_games;
        queue<PGG*> fifo_games;
        if(fix_players_per_game) {
            players_per_game = config::nPlayers/(float)config::nGames * config::games_per_player;

            //generate vector of game IDs
            vector<PGG*> game_IDs;
            for(auto& g: *vGames) {
                game_IDs.push_back(&g);
            }

            //generate shuffled vectors
            vector<PGG*> shuffle = game_IDs;
            for(int i=0; i < players_per_game; i++) {
                std::random_shuffle(shuffle.begin(), shuffle.end());
                for(auto& s: shuffle) {
                    fifo_games.push(s);
                }
            }
        }

        int iP = 0;
        for(Player& player: *vPlayers) {
            player.sPGG.clear();
            if(fix_players_per_game) {
                //assign players to games
                for(int k=0; k < games_per_player; k++) {
                    if(player.sPGG.find(fifo_games.front()) != player.sPGG.end()) {
                        fifo_games.push(fifo_games.front());
                        fifo_games.pop();
                        k--;
                    } else {
                        player.sPGG.insert(fifo_games.front());
                        if(k == 0) player.focal_game = fifo_games.front();
                        fifo_games.front()->sPlayers.insert(&player);
                        fifo_games.pop();
                    }
                }
                if(player.sPGG.size() < 5) cout << "-.- " << iP << endl;
                ++iP;
            } else {
                //as long as there are not enough games for the player: add a random game
                int tmp_games_per_player = v_nGames_per_player.back();
                v_nGames_per_player.pop_back();
                while(player.sPGG.size() < tmp_games_per_player) {
                    PGG* game = &(*vGames)[rnd_game(rng)]; //random game
                    player.sPGG.insert(game);
                    game->sPlayers.insert(&player);          //add the player to the game
                }
            }
            stats::this_exp->mean_games_per_player += player.sPGG.size();
        }
        stats::this_exp->mean_games_per_player = stats::this_exp->mean_games_per_player / (double)vPlayers->size();
    }
}

//gnerates strategy update network
//(nPlayers * games_per_player)
void assignNeighbours(vector<Player>* vPlayers, Neighbours_Mode Mode) {
    stats::this_exp->mean_nNeighbours = 0.;

    if(Mode == Neighbours_Mode::SAMEGAME) {
        for(Player& player: *vPlayers) {
            player.sNeighbours.clear();
            //fill enighbours from all gmes, the palyer participates in
            for(auto& g: player.sPGG) {
               player.sNeighbours.insert(g->sPlayers.begin(), g->sPlayers.end());
            }
            stats::this_exp->mean_nNeighbours += player.sNeighbours.size();
            //cout << player.sNeighbours.size() << endl;
        }
    } else if (Mode == Neighbours_Mode::RANDOM) {
        boost::random::uniform_int_distribution<> rnd_neighbour(0,config::nPlayers-1);
        for(Player& player: *vPlayers) {
            player.sNeighbours.clear();
            for(int i=0; i<config::nNeighbours_random; i++) {
                player.sNeighbours.insert(&(*vPlayers)[rnd_neighbour(rng)]);
            }
            stats::this_exp->mean_nNeighbours += player.sNeighbours.size();
        }
    } else if (Mode == Neighbours_Mode::FOCAL_GAME) {
        for(Player& player: *vPlayers) {
            player.sNeighbours.clear();
            //select focal game. Use a random one for non Lattice setups
            PGG* g = player.focal_game;
            player.sNeighbours.insert(g->sPlayers.begin(), g->sPlayers.end());
            stats::this_exp->mean_nNeighbours += player.sNeighbours.size();
            //cout << player.sNeighbours.size() << endl;
        }

    }
    stats::this_exp->mean_nNeighbours = stats::this_exp->mean_nNeighbours / (double) config::nPlayers;
}

//assignes payoffs to every player for one round
// O(nGames * 2* nPlayers/nGames*games_per_player)
//      = 2*O(nGames * games_per_player) + 2*O(nPlayers)
//
// SINGLE_CONTRIBUTION: If there is one cooperator in the game: Everybody gets benefit
//                      all cooperators pay costs
//                       --> one cooperator is enough
//                       --> many games: more cooperators
// MULTI_CONTRIBUTION: Each player can contribute. Everybody gets (benefit / Players in the game)
//                      all cooperators pay costs
//                      --> many players per game: harder to cooperate
void playRound(vector<PGG>* vGames, vector<Player>* vPlayers, Game_Mode Mode, int round)  {
    //reset payoffs
    for(Player& p: *vPlayers) p.payoff = 0;

    if(Mode == Game_Mode::SINGLE_CONTRIBUTION) {
        for(PGG& game: *vGames) {
            //see if someone cooperates in this game
            bool at_least_one_cooperator = false;
            for(auto& player: game.sPlayers) {
                if(player->cooperate) {
                    at_least_one_cooperator = true;
                    stats::this_round->nPGG_with_cooperator++;
                    break;
                }
            }

            for(auto& player: game.sPlayers) {
               if(player->cooperate) {
                   player->payoff += game.benefit - (game.costs + player->costs_bias);
               } else if(at_least_one_cooperator) {
                   player->payoff += game.benefit;
               } else {
                   player->payoff += 0;
               }
            }
        }
    } else
    if(Mode == Game_Mode::MULTI_CONTRIBUTION) {
        if(debug::game_details) cout << "Games: " << endl;
        for(PGG& game: *vGames) {
            bool at_least_one_cooperator = false;
            double game_payoff = 0;
            double game_benefit = game.benefit / (float)game.sPlayers.size();
            //double game_benefit = game.benefit;
            //calculate value of the game and subtract costs for cooperating
            int nCooperators_game = 0;
            for(auto& player: game.sPlayers) {
                if(player->cooperate) {
                    ++nCooperators_game;
                    stats::this_exp->mean_cooperates_per_player[player->ID]
                        += 1./(float)config::nRounds/(float)player->sPGG.size();
                    at_least_one_cooperator = true;
                    //player->payoff -= (game.costs + game_benefit); //does not profit by own good
                    player->payoff -= (game.costs + player->costs_bias);
                    game_payoff += game_benefit;
                }
            }

            stats::this_round->cooperators_per_game.push_back(nCooperators_game / (float)game.sPlayers.size());
            stats::this_exp->mean_cooperators_per_game[game.ID] += nCooperators_game / (float)game.sPlayers.size() / (float)config::nRounds;

            if(at_least_one_cooperator) stats::this_round->nPGG_with_cooperator++;

            if(debug::game_details) {
                cout << game.ID << ":\tcooperators: " << nCooperators_game;
                cout << " (" << nCooperators_game / (float)game.sPlayers.size() * 100. << "\%) \t";
                cout << "total: " << game.sPlayers.size() << endl;
                cout << "\tpayoff: " << game_payoff << endl;
                cout << endl;
            }

            for(auto& player: game.sPlayers) {
                player->payoff += game_payoff;
            }
        }
        if(debug::game_details) cout << endl;
    }
    //average payoffs
    for(Player & p: *vPlayers) {
        p.payoff = p.payoff / (double) p.sPGG.size();
        //cout << p.payoff << endl;
        stats::this_round->mean_payoff += p.payoff;
        if(p.cooperate) {
            stats::this_round->nCooperators++;
            stats::this_round->mean_payoff_C += p.payoff;
        } else {
            stats::this_round->mean_payoff_D += p.payoff;
        }
        if(debug::game_details) {
            cout << "Cooperates: " << p.cooperate << " -> " << p.payoff << endl;
            for(PGG* g: p.sPGG) {
                cout << g->ID << " ";
            }
            cout << endl;
        }
    }

    stats::this_round->mean_payoff_C = stats::this_round->mean_payoff_C / (float)stats::this_round->nCooperators;
    stats::this_round->mean_payoff_D = stats::this_round->mean_payoff_D
                                    / (float)(config::nPlayers -stats::this_round->nCooperators);

    stats::this_round->mean_payoff = stats::this_round->mean_payoff / (float)config::nPlayers;
    stats::this_exp->vPayoffs.push_back(stats::this_round->mean_payoff);
    stats::this_exp->vCooperators.push_back(stats::this_round->nCooperators);

    if(debug::round_stats) {
        cout << "Round # " << round << endl;
        cout << stats::this_round->nCooperators << " (" << stats::this_round->nCooperators / (float)config::nPlayers * 100.;
        cout << "\%) cooperators" << endl;
        cout << "PGG with at least one cooperator: " << stats::this_round->nPGG_with_cooperator;
        cout << " (" << stats::this_round->nPGG_with_cooperator / (double)config::nGames*100 << "\%)" << endl;
        cout << "Mean payoff per game: " << stats::this_round->mean_payoff << endl;
        cout << "Mean payoff per C: " << stats::this_round->mean_payoff_C << endl;
        cout << "Mean payoff per D: " << stats::this_round->mean_payoff_D << endl;
        cout << endl;
    }
}

// O(nPlayers * nNeighbours) = O(nPlayers * games_per_player * players_per_game)
//      = O(nPlayers * games_per_player * nPlayers * games_per_player / nGames)
//      = O (nPlayers^2 * games_per_player^2 / nGames)
// XXX: WARNING: Just SMOOTHED_IMMITATION and BEST_IMITATION implemented properly
void appendUpdateDetails(vector<vector<float> >& data);
void updateStrategies(vector<Player>* vPlayers, Strategy_Update_Mode Mode) {

    vector<Player*> vChangeStrategy;
    vector<vector<float> > UpdateData;

    //Maybe delete, not a good method...
    if(Mode == Strategy_Update_Mode::SAMEPROB) {
        for(auto& player: (*vPlayers)) {
            //int tmp2 = 0;
            float rand_ref = rnd_uniform(rng);
            float cummulative_probability = 0;
            float nNeighbours = player.sNeighbours.size();
            float neighboursLeft = nNeighbours;
            for(Player* neighbour: player.sNeighbours) {
                //probability to adopt the new strategy

                float p = 1. / nNeighbours / (1. + exp((player.payoff -
                    neighbour->payoff)*config::updateStrength)*config::updateAversion);

                cummulative_probability += p;

                if(cummulative_probability > rand_ref) { //adopt strategy
                    vChangeStrategy.push_back(&player);
                    break;
                }
                //++tmp2;
                --neighboursLeft;
                if((cummulative_probability + neighboursLeft/nNeighbours) < rand_ref) break;
            }
            assert(cummulative_probability < 1);
            //cout << "p: " << cummulative_probability << endl;
        }

    } else
    if(Mode == Strategy_Update_Mode::SMOOTHED_IMMITATION) {
        vector<float> data;
        for(auto& player: (*vPlayers)) {
            //if(rnd_uniform(rng) > 0.5) continue;
            float average_payoff_same = 0.;
            float average_payoff_other = 0.;
            int n_same = 0;
            int n_other = 0;
            float neighbour_CC = 0.;
            float neighbour_DC = 0.;
            float neighbour_DD = 0.;
            float neighbour_CD = 0.;
            for(Player* neighbour: player.sNeighbours) {
                if(player.cooperate == neighbour->cooperate) {
                    average_payoff_same += neighbour->payoff;
                    ++n_same;
                    if(player.cooperate) neighbour_CC += 1.;
                    else neighbour_DD += 1.;
                } else {
                    average_payoff_other += neighbour->payoff;
                    ++n_other;
                    if(player.cooperate) neighbour_CD += 1.;
                    else neighbour_DC += 1.;
                }
            }
            stats::this_round->pNeighbourCC += neighbour_CC / player.sNeighbours.size();
            stats::this_round->pNeighbourCD += neighbour_CD / player.sNeighbours.size();
            stats::this_round->pNeighbourDD += neighbour_DD / player.sNeighbours.size();
            stats::this_round->pNeighbourDC += neighbour_DC / player.sNeighbours.size();

            if(n_same > 0) average_payoff_same = average_payoff_same / n_same;
            else average_payoff_same = 0;
            if(n_other > 0) average_payoff_other = average_payoff_other / n_other;
            else average_payoff_other = 0;

            if(player.ID < 10000 && iR < 200&& stats::fWriteUpdt) {
                data.clear();
                data.push_back(player.payoff - average_payoff_other);
                data.push_back(0);
            }

            float p = 1. / (1. + exp((player.payoff -
                    average_payoff_other)
                    *config::updateStrength)*config::updateAversion);
            if(n_other == 0) p = -1.;
            bool had_update = false;
            if(p>rnd_uniform(rng)) {
                vChangeStrategy.push_back(&player);
                had_update = true;
                ++stats::this_exp->updates_per_player[player.ID];
            }
            if(player.ID < 10000 && iR < 200&& stats::fWriteUpdt) {
                if(had_update) data[1] = 1;
                UpdateData.push_back(data);
            }
        }
    } else if(Mode == Strategy_Update_Mode::BEST_IMITATION) {
        vector<float> data;
        for(auto& player: (*vPlayers)) {
            float neighbour_CC = 0.;
            float neighbour_DC = 0.;
            float neighbour_DD = 0.;
            float neighbour_CD = 0.;
            if(player.ID < 10000 && iR < 200 && stats::fWriteUpdt) {
                data.clear();
                data.push_back(0);
                data.push_back(0);
            }

            float best_payoff = 0.;
            bool best_strategy = false;
            for(Player* neighbour: player.sNeighbours) {
                if(neighbour->payoff > best_payoff) {
                    best_payoff = neighbour->payoff;
                    best_strategy = neighbour->cooperate;
                }

                //statistics
                if(player.cooperate == neighbour->cooperate) {
                    if(player.cooperate) neighbour_CC += 1.;
                    else neighbour_DD += 1.;
                } else {
                    if(player.cooperate) neighbour_CD += 1.;
                    else neighbour_DC += 1.;
                }
            }

            //mark for update
            bool had_update = false;
            if(best_payoff > player.payoff && best_strategy != player.cooperate) {
                vChangeStrategy.push_back(&player);
                ++stats::this_exp->updates_per_player[player.ID];
                had_update = true;
            }
            if(player.ID < 10000 && iR < 200 && stats::fWriteUpdt) {
                if(had_update || best_strategy == player.cooperate) data[1] = 1;
                data[0] = player.payoff - best_payoff;
                UpdateData.push_back(data);
            }

            stats::this_round->pNeighbourCC += neighbour_CC / player.sNeighbours.size();
            stats::this_round->pNeighbourCD += neighbour_CD / player.sNeighbours.size();
            stats::this_round->pNeighbourDD += neighbour_DD / player.sNeighbours.size();
            stats::this_round->pNeighbourDC += neighbour_DC / player.sNeighbours.size();
        }
    }

    stats::this_round->pNeighbourCC = stats::this_round->pNeighbourCC / vPlayers->size();
    stats::this_round->pNeighbourCD = stats::this_round->pNeighbourCD / vPlayers->size();
    stats::this_round->pNeighbourDD = stats::this_round->pNeighbourDD / vPlayers->size();
    stats::this_round->pNeighbourDC = stats::this_round->pNeighbourDC / vPlayers->size();

    //update strategies simultaneously
    for(auto p: vChangeStrategy) {
        if(p->cooperate) {
            stats::this_round->switch_to_defect++;
        } else {
            stats::this_round->switch_to_cooperate++;
        }
        p->cooperate = not p->cooperate;
    }

    if(iR < 400 && iExp == 0 && stats::fWriteUpdt) appendUpdateDetails(UpdateData);

    //assert(vUpdatedPlayers.size() == vPlayers->size() && vUpdatedPlayers.size() == config::nPlayers);

    if(debug::round_strategy_updates) {
        cout << "before: " << stats::this_round->nCooperators << " cooperators" << endl;
        cout << "c->d: " << stats::this_round->switch_to_defect << "\t d->c: " << stats::this_round->switch_to_cooperate;
        cout << "\t effective: " << stats::this_round->switch_to_cooperate - stats::this_round->switch_to_defect << endl;
        cout << endl;
    }
}

// O(nRounds * nPlayers^2 * games_per_player^2 / nGames)
void writePlayerDetails(vector<Player>* vPlayers);
void simulate(vector<Player>* vPlayers, vector<PGG>* vGames, int nRounds) {
    cout << "Simulating " << nRounds << " rounds" << endl;
    for(int i=0; i<nRounds; i++) {
        stats::Round R;
        stats::this_exp->vR.push_back(R);
        iR = i;
        stats::this_round = &stats::this_exp->vR[iR];
        if(i < 50) writePlayerDetails(vPlayers); //for plotting lattices
        playRound(vGames, vPlayers, config::game_mode, i);
        updateStrategies(vPlayers, config::strategy_update_mode);
    }
}

void writeOutput() {
    stringstream ss_filename;
    ss_filename << config::ss_path.str() << "/cooperators.dat";

    ofstream outf(ss_filename.str().c_str());
    if(!outf) {cerr << "Oh Oh, results.dat could not be opened for writing!" << endl; exit(1);}

    outf << setprecision(6);
    outf << "round\tcooperators\tpNeighboursCC\tpNeighboursCD\tpNeighboursDD\tpNeighboursDC" << endl;
    for(int i=0; i<stats::vMean_cooperators.size(); i++) {
        outf << i << "\t" << stats::vMean_cooperators[i] / (double)config::nPlayers;

        /*
        float mean_CC = 0.;
        float mean_CD = 0.;
        float mean_DD = 0.;
        float mean_DC = 0.;

        for(int iE = 0; iE < stats::vExp.size(); iE++) {
           mean_CC += stats::vExp[iE].vR[i].pNeighbourCC;
           mean_CD += stats::vExp[iE].vR[i].pNeighbourCD;
           mean_DD += stats::vExp[iE].vR[i].pNeighbourDD;
           mean_DC += stats::vExp[iE].vR[i].pNeighbourDC;
        }
        outf << "\t" << mean_CC / stats::vExp.size();
        outf << "\t" << mean_CD / stats::vExp.size();
        outf << "\t" << mean_DD / stats::vExp.size();
        outf << "\t" << mean_DC / stats::vExp.size();
        */

        outf << "\t" << stats::vExp[0].vR[i].pNeighbourCC;
        outf << "\t" << stats::vExp[0].vR[i].pNeighbourCD;
        outf << "\t" << stats::vExp[0].vR[i].pNeighbourDD;
        outf << "\t" << stats::vExp[0].vR[i].pNeighbourDC;

        outf << endl;
    }

    cout << "Data was written to data/results.dat" << endl;
}

void writeGameDetails(vector<PGG>* vGames) {
    stringstream ss_filename;
    stringstream ss_exp_path;
    ss_exp_path << config::ss_path.str() << "/exp_" << iExp;
    mkdir(ss_exp_path.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    ss_filename << ss_exp_path.str() << "/games.dat";

    ofstream outf(ss_filename.str().c_str());
    if(!outf) {cerr << "Oh Oh, " <<  ss_filename.str() << " could not be opened for writing!" << endl; exit(1);}

    outf << "ID\tnPlayers\tcosts\tCoop\tendCoop" << endl;
    for(auto& game: *vGames) {
        outf << game.ID << "\t" << game.sPlayers.size() << "\t" << game.costs;
        outf << "\t" << stats::this_exp->mean_cooperators_per_game[game.ID];
        outf << "\t" << stats::this_exp->vR[config::nRounds-1].cooperators_per_game[game.ID] << endl;
    }
}

void writePlayerDetails(vector<Player>* vPlayers) {
    stringstream ss_filename;
    stringstream ss_exp_path;
    ss_exp_path << config::ss_path.str() << "/exp_" << iExp;
    mkdir(ss_exp_path.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if(iR == config::nRounds - 1) {
        ss_filename << ss_exp_path.str() << "/players.dat";
    } else {
        ss_filename << ss_exp_path.str() << "/players_" << iR << ".dat";
    }

    ofstream outf(ss_filename.str().c_str());
    if(!outf) {cerr << "Oh Oh, " <<  ss_filename.str() << " could not be opened for writing!" << endl; exit(1);}

    outf << "ID\tnGames\tnUpdates\tmeanCoop\tendCoop" << endl;
    for(auto& player: *vPlayers) {
        outf << player.ID << "\t" << player.sPGG.size() << "\t" << stats::this_exp->updates_per_player[player.ID];
        outf << "\t" << stats::this_exp->mean_cooperates_per_player[player.ID];
        outf << "\t" <<player.cooperate << endl;
    }
}

void writeUpdateDetails() {
    stringstream ss_filename;
    stringstream ss_exp_path;
    ss_exp_path << config::ss_path.str() << "/exp_" << iExp;
    mkdir(ss_exp_path.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    ss_filename << ss_exp_path.str() << "/updateDetails.dat";

    ofstream outf(ss_filename.str().c_str(), std::ofstream::out);
    if(!outf) {cerr << "Oh Oh, " <<  ss_filename.str() << " could not be opened for writing!" << endl; exit(1);}
    outf << "payoffDiff\tswitch" << endl;
}

void appendUpdateDetails(vector<vector<float> >& data) {
    stringstream ss_filename;
    stringstream ss_exp_path;
    ss_exp_path << config::ss_path.str() << "/exp_" << iExp;
    mkdir(ss_exp_path.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    ss_filename << ss_exp_path.str() << "/updateDetails.dat";

    ofstream outf(ss_filename.str().c_str(), std::ofstream::out | std::ofstream::app);
    if(!outf) {cerr << "Oh Oh, " <<  ss_filename.str() << " could not be opened for writing!" << endl; exit(1);}
    for(auto& e: data) {
        outf << e[0] << "\t" << e[1] << endl;
    }
}

void print_config() {
    cout << endl;
    cout << "This is ** RUN " << config::run << " **" << endl;
    cout << "------------------------------" << endl;
    cout << "Number of experiments: \t" << config::nExperiments << endl;
    cout << "Number of rounds: \t" << config::nRounds << endl;
    cout << "Number of players: \t" << config::nPlayers << endl;
    cout << "Number of games: \t" << config::nGames << endl;
    cout << endl;
    cout << "Mean games per player: \t" << config::games_per_player << endl;
    cout << "Game assignment node: \t" << (int)config::game_assignment_mode << endl;
    cout << endl;
    cout << "Initial coop. prob.: \t" << config::pCoop*100 << " \%" << endl;
    cout << endl;
    cout << "PGG costs: \t" << config::costs_min << " - " << config::costs_max << endl;
    cout << "PGG enefit: \t" << config::benefit_min << " - " << config::benefit_max << endl;
    cout << endl;
    cout << "Number of random neighbours: " << config::nNeighbours_random << endl;
    cout << "Update strength: " << config::updateStrength << endl;
    cout << "neighbours_mode: " << (int)config::neighbours_mode << endl;
    cout << "------------------------------" << endl;
    cout << endl;
}

void writeConfig() {
    //TODO: Implement boost filesystem
    stringstream ss_filename;
    ss_filename << config::ss_path.str() << "/config.dat";

    //boost::filesystem::path dir(ss_path.str());

    //boost::filesystem::create_directory(dir);
    //if(not boost::filesystem::create_directory(dir)){cerr << "Oh, Oh, could not create dir" << ss_path.str();}

    //ofstream outf(ss_filename.str().c_str());
    ofstream outf(ss_filename.str().c_str());
    if(!outf) {cerr << "Oh Oh, config.dat could not be opened for writing!" << endl; exit(1);}
    outf << "Run\t" << config::run << endl;
    outf << "nRuns\t" << config::nRuns << endl;
    outf << "nExperiments\t" << config::nExperiments << endl;
    outf << "nRounds\t" << config::nRounds << endl;
    outf << "nPlayers\t" << config::nPlayers << endl;
    outf << "nGames\t" << config::nGames << endl;
    outf << "games_per_player\t" << config::games_per_player << endl;
    outf << "game_assignment_mode\t" << (int)config::game_assignment_mode << endl;
    outf << "pCoop\t" << config::pCoop << endl;
    outf << "costs_min\t" << config::costs_min << endl;
    outf << "costs_max\t" << config::costs_max << endl;
    outf << "costs_het\t" << config::costs_het << endl;
    outf << "f_het_c_games\t" << config::f_het_c_games << endl;
    outf << "f_het_c_players\t" << config::f_het_c_players << endl;
    outf << "scan_step_costs\t" << config::scan_step_costs << endl;
    outf << "benefit_min\t" << config::benefit_min << endl;
    outf << "benefit_max\t" << config::benefit_max << endl;
    outf << "nNeighbours_random\t" << config::nNeighbours_random << endl;
    outf << "updateStrength\t" << config::updateStrength << endl;
    outf << "updateAversion\t" << config::updateAversion << endl;
    outf << "neighbours_mode\t" << (int)config::neighbours_mode << endl;
    outf << "game_mode\t" << (int)config::game_mode << endl;
    outf << "strategy_update_mode\t" << (int)config::strategy_update_mode << endl;

    //cout << "Configuration was written to" << ss_filename.str() << endl;

}

//TODO: Assertions
int main() {
    cout << "Starting main.cpp ..." << endl;

    config::run = 200;
    config::scan = 100;
    config::nRuns = 30;
    config::nExperiments = 10;
    config::nRounds = 500;         //Number of players
    config::nPlayers = 2500;         //Number of players
    config::nGames = 2500;            //Number of games
    config::games_per_player = 5;   //Number of average games a player participates in
    config::pCoop = 0.5;            //Initial probability to cooperate
    config::benefit_min = 1;
    config::benefit_max = 1;
    config::updateStrength = 10.;   // 10
    config::updateAversion = 20.;   // 20
    config::nNeighbours_random = 5;
    config::f_het_c_games = false;
    config::f_het_c_players = false;
    config::costs_het = 0.0;
    config::neighbours_mode         = Neighbours_Mode::FOCAL_GAME;
    config::game_assignment_mode    = Game_Assignment_Mode::LATTICE;
    config::game_mode               = Game_Mode::MULTI_CONTRIBUTION;
    config::strategy_update_mode    = Strategy_Update_Mode::BEST_IMITATION;

    config::scan_step_costs = 0.020;
    float players_per_game = config::nPlayers/config::nGames * config::games_per_player;
    if(config::nRuns == 1) {
        config::costs_min = 0.10;
        config::costs_max = 0.10;
    } else {
        config::costs_min = 1./players_per_game - config::nRuns*config::scan_step_costs/2.;
        config::costs_max = 1./players_per_game - config::nRuns*config::scan_step_costs/2.; //heterogeneity
    }
    if(config::f_het_c_games) {
        config::costs_min -= config::costs_het;
        config::costs_max += config::costs_het;
    }
    //config::ss_path << "data/run_" << config::run;


    if(config::nRuns > 1) {
        config::ss_path << WD << "data/scan_" << config::scan << "/run_" << config::run;
        stringstream tmp_ss;
        tmp_ss << WD << "data/scan_" << config::scan;
        mkdir(tmp_ss.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    } else {
        config::ss_path << WD << "data/run_" << config::run;
    }

    for(int iRun = 0; iRun < config::nRuns; iRun++) {
        config::ss_path.str( std::string() );
        config::ss_path.clear();
        if(config::nRuns > 1) {
             debug::round_stats = false;
             config::ss_path << WD << "data/scan_" << config::scan << "/run_" << config::run;
         } else {
             config::ss_path << WD << "data/run_" << config::run;
         }

        mkdir(config::ss_path.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

        stats::reset();

        cout << setprecision(4);

        if(debug::config) {
            print_config();
        }

        for(int i=0; i<config::nExperiments; i++) {
            stats::Experiment stats_exp;
            stats::vExp.push_back(stats_exp);
            iExp = i;
            stats::this_exp = &stats::vExp[iExp];
            if(stats::fWriteUpdt) writeUpdateDetails();

            vector<Player> vPlayers = generatePlayers(config::nPlayers, config::pCoop);
            //vPlayers = initialCoopDistribution(vPlayers);
            vector<PGG> vGames      = generateGames(config::nGames, config::costs_min, config::costs_max,
                                                        config::benefit_min, config::benefit_max);

            //Mode: EQUAL, POISSON, BIASED_POISSON
            assignPlayersToGames(&vPlayers, &vGames, config::games_per_player,
                        config::game_assignment_mode);
            //Mode: SAMEGAME, RANDOM
            assignNeighbours(&vPlayers, config::neighbours_mode);
            simulate(&vPlayers, &vGames, config::nRounds);

            //stats::vCooperators.push_back(stats::this_exp->vCooperators);

            writeGameDetails(&vGames);
            writePlayerDetails(&vPlayers);

            if(debug::experiment_stats) {

                stats::this_exp->mean_players_per_game = 0;
                for(auto& g : vGames) {stats::this_exp->mean_players_per_game += g.sPlayers.size();}
                stats::this_exp->mean_players_per_game = stats::this_exp->mean_players_per_game / (float)config::nGames;

                cout << "Summary of Experiment # " << i << endl;
                cout << "Total numbers of players: " << config::nPlayers << " with ";
                cout << stats::this_exp->initial_cooperators << " (";
                cout << stats::this_exp->initial_cooperators/(float)config::nPlayers*100<<"\%) of init cooperators" << endl;
                cout << "average number of neighbours: " << stats::this_exp->mean_nNeighbours << endl;
                cout << "average number of games per player: " << stats::this_exp->mean_games_per_player << endl;
                cout << "average number of players per game: " << stats::this_exp->mean_players_per_game << endl;
                //average cooperators over last 50 rounds
                stats::this_exp->nCooperators_stable = 0;
                for(int i=config::nRounds/2; i<config::nRounds; i++) {
                    stats::this_exp->nCooperators_stable += stats::this_exp->vCooperators[i];
                }
                stats::this_exp->nCooperators_stable = stats::this_exp->nCooperators_stable / config::nRounds*2;
                cout << "In the last " << config::nRounds/2 << " rounds: ";
                cout << stats::this_exp->nCooperators_stable << " cooperators." << endl;
                cout << endl;
            }
        }

        for(int r=0; r<config::nRounds; r++) {
            float mean = 0;
            for(int i=0; i<config::nExperiments; i++) {
                mean += stats::vExp[i].vR[r].nCooperators;
            }
            stats::vMean_cooperators.push_back(mean / (float)config::nExperiments);
        }

        writeOutput();
        writeConfig();

        ++config::run;
        config::costs_min += config::scan_step_costs;
        config::costs_max += config::scan_step_costs;
    }

    return 0;
}
